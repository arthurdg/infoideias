﻿<?php
	if(isset($_GET['numero']))
	{
		primoInferior($_GET['numero']);
	}
	function primoInferior($numero)
	{
		if($numero<0)
		{
			$numero = 0;
		}
		else
		{
			while($numero%2==0 || $numero%3==0)
			{
				if($numero == 2 || $numero == 3)
				{
					break;
				}
				$numero=floor($numero-1);
			}
		}
		echo 'O número primo inferior mais próximo é:'.$numero;
	}		
?>
<!Doctype html>
	<head>
		<title> Função Primo Inferior </title>
	</head>
	<body>
		<form action="index.php" method="get">
			<input type="text" name="numero" placeholder="Informe o número">
			<input type="submit" name="calcular" value="Calcular">
			<br/>
			<br/>
			<br/>
		</form>
	</body>
</html>